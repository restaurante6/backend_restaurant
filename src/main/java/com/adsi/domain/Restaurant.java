package com.adsi.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    private String name;

    private String description;

    private String address;

    @Lob
    @Column(name = "image")
    private Byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;
}

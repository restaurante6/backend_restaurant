package com.adsi.service.transformer;

import com.adsi.domain.Board;
import com.adsi.service.DTO.BoardDto;

public class BoardTransformer {

    public static BoardDto getBoardDTOFromBoard (Board board){
        if (board==null){
            return null;
        }

        BoardDto dto = new BoardDto();
        dto.setId(board.getId());
        dto.setCapacity(board.getCapacity());
        dto.setNumber(board.getNumber());
        dto.setIdRestaurante(board.getIdRestaurante());
        return dto;
    }

    public static Board getBoardFromBoardDTO (BoardDto dto){
        if (dto == null){
            return null;
        }

        Board board =new Board();
        board.setId(dto.getId());
        board.setCapacity(dto.getCapacity());
        board.setNumber(dto.getNumber());
        board.setIdRestaurante(dto.getIdRestaurante());
        return board;
    }
}

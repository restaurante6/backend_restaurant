package com.adsi.service.transformer;

import com.adsi.domain.Restaurant;
import com.adsi.service.DTO.RestaurantDto;

public class RestaurantTransformer {
    public static RestaurantDto getRestaurantDTOFromRestaurant (Restaurant restaurant){
        if ( restaurant==null){
            return null;
        }

RestaurantDto dto = new RestaurantDto();

        dto.setId(restaurant.getId());
        dto.setName(restaurant.getName());
        dto.setDescription(restaurant.getDescription());
        dto.setAddress(restaurant.getAddress());
        dto.setImage(restaurant.getImage());
        dto.setImageContentType(restaurant.getImageContentType());
        return dto;
    }

    public static Restaurant getRestaurantFromRestaurantDTO (RestaurantDto dto){
        if (dto == null){
            return null;
        }
        Restaurant restaurant =new Restaurant();

        restaurant.setId(dto.getId());
        restaurant.setName(dto.getName());
        restaurant.setDescription(dto.getDescription());
        restaurant.setAddress(dto.getAddress());
        restaurant.setImage(dto.getImage());
        restaurant.setImageContentType(dto.getImageContentType());
        return restaurant;
    }

}

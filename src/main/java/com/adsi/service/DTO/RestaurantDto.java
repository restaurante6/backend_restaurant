package com.adsi.service.DTO;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestaurantDto {

    private int id;

    private String name;

    private String description;

    private String address;


    private Byte[] image;


    private String imageContentType;
}

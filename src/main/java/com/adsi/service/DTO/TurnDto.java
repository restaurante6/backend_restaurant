package com.adsi.service.DTO;

import com.adsi.domain.Restaurant;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class TurnDto implements Serializable {

    private int id;

    @NotEmpty(message = "este campo no puede ser vacio")
    private String name;


    private Restaurant idRestaurant;
}

package com.adsi.service.DTO;

import com.adsi.domain.Restaurant;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Getter
@Setter

public class BoardDto implements Serializable {

    private int id;
    @NotNull
    @Min(1)
    private int capacity;
    @NotNull
    @Min(1)
    private int number;

    private Restaurant idRestaurante;
}

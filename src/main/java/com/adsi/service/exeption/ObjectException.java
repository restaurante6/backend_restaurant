package com.adsi.service.exeption;

import lombok.Getter;



@Getter
public class ObjectException extends RuntimeException{

    private static final Long serialVersionUID = 1L;
    private final ObjectError objectError;

    public ObjectException(String message, ObjectError objectError){
        super(message);
        this.objectError = objectError;
    }



}

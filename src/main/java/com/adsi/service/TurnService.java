package com.adsi.service;

import com.adsi.service.DTO.TurnDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface TurnService {

    TurnDto create(TurnDto turnDto);
    Page<TurnDto>findAll(Pageable pageable);
    TurnDto getByid(int id);
}

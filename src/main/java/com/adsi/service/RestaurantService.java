package com.adsi.service;

import com.adsi.service.DTO.RestaurantDto;

import java.util.List;
import java.util.Optional;

public interface RestaurantService {

public List<RestaurantDto>getAllRestaurant();

public RestaurantDto create (RestaurantDto restaurantDto);

RestaurantDto update (RestaurantDto restaurantDto);

 Optional<RestaurantDto> getById (int id);

 void delite (int id);

 Boolean existsByName (String name);

}

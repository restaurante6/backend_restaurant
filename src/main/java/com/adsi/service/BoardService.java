package com.adsi.service;

import com.adsi.service.DTO.BoardDto;

import java.util.List;
import java.util.Optional;

public interface BoardService {

    List<BoardDto>read ();

    BoardDto create (BoardDto boardDto);

    BoardDto update (BoardDto boardDto);

    Optional<BoardDto> getById (int id);

    void delete (int id);


}

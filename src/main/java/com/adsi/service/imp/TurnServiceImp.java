package com.adsi.service.imp;

import com.adsi.domain.Turn;
import com.adsi.repository.TurnRespository;
import com.adsi.service.DTO.TurnDto;
import com.adsi.service.TurnService;
import com.adsi.service.exeption.ObjectError;
import com.adsi.service.exeption.ObjectException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TurnServiceImp implements TurnService {

    @Autowired
    TurnRespository turnRespository;

    public static final ModelMapper modelMapper= new ModelMapper();

    @Override
    public TurnDto create(TurnDto turnDto) {
        if (turnDto.getName()==""){
            throw new ObjectException("el nombre no debe ser vacio",
                    new ObjectError(HttpStatus.BAD_REQUEST, "el campo nombre esta vacio"));

        }
        return modelMapper.map(turnRespository.save(modelMapper.map(turnDto, Turn.class)),TurnDto.class);
    }

    @Override
    public Page<TurnDto> findAll(Pageable pageable) {

        return turnRespository.findAll(pageable).map(turn -> modelMapper.map(turn, TurnDto.class));
    }

    @Override
    public TurnDto getByid(int id) {
        Optional<Turn> optionalTurn = turnRespository.findById(id);
        if (!optionalTurn.isPresent()){
            throw new ObjectException("el turno no fue creado",
                    new ObjectError(HttpStatus.NOT_FOUND, "el trurno con id: "+id+"no fue creado"));
        }
        return modelMapper.map(optionalTurn.get(), TurnDto.class);
    }
}

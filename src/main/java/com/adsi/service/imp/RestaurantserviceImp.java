package com.adsi.service.imp;

import com.adsi.domain.Restaurant;
import com.adsi.repository.RestaurantRepository;
import com.adsi.service.DTO.RestaurantDto;
import com.adsi.service.RestaurantService;
import com.adsi.service.transformer.RestaurantTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RestaurantserviceImp implements RestaurantService {

    @Autowired
    RestaurantRepository restaurantRepository;

    @Override
    public List<RestaurantDto> getAllRestaurant() {
        return restaurantRepository.findAll()
                .stream()
                .map(RestaurantTransformer::getRestaurantDTOFromRestaurant)
                .collect(Collectors.toList());
    }

    @Override
    public RestaurantDto create(RestaurantDto restaurantDto) {
        Restaurant restaurant = RestaurantTransformer.getRestaurantFromRestaurantDTO(restaurantDto);
        return RestaurantTransformer.getRestaurantDTOFromRestaurant(restaurantRepository.save(restaurant));
    }

    @Override
    public RestaurantDto update(RestaurantDto restaurantDto) {
        Restaurant restaurant = RestaurantTransformer.getRestaurantFromRestaurantDTO(restaurantDto);
        return RestaurantTransformer.getRestaurantDTOFromRestaurant(restaurantRepository.save(restaurant));
    }

    @Override
    public Optional<RestaurantDto> getById(int id) {
        return restaurantRepository.findById(id)
                .map(RestaurantTransformer::getRestaurantDTOFromRestaurant);
    }

    @Override
    public void delite(int id) {
    restaurantRepository.deleteById(id);
    }

    @Override
    public Boolean existsByName(String name) {
        return restaurantRepository.existsByName(name);
    }
}

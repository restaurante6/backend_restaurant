package com.adsi.service.imp;

import com.adsi.domain.Board;
import com.adsi.repository.BoardRepository;
import com.adsi.service.BoardService;
import com.adsi.service.DTO.BoardDto;
import com.adsi.service.transformer.BoardTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BoardImp implements BoardService {
  @Autowired
    BoardRepository boardRepository;

    @Override
    public List<BoardDto> read() {
        return boardRepository.findAll()
                .stream()
                .map(BoardTransformer::getBoardDTOFromBoard)
                .collect(Collectors.toList());
    }

    @Override
    public BoardDto create(BoardDto boardDto) {
        Board board = BoardTransformer.getBoardFromBoardDTO(boardDto);
        return BoardTransformer.getBoardDTOFromBoard(boardRepository.save(board));
    }

    @Override
    public BoardDto update(BoardDto boardDto) {
        Board board = BoardTransformer.getBoardFromBoardDTO(boardDto);
        return BoardTransformer.getBoardDTOFromBoard(boardRepository.save(board));
    }

    @Override
    public Optional<BoardDto> getById(int id) {
        return boardRepository.findById(id)
                .map(BoardTransformer::getBoardDTOFromBoard);
    }

    @Override
    public void delete(int id) {
        boardRepository.deleteById(id);

    }
}

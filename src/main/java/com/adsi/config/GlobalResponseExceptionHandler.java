package com.adsi.config;

import com.adsi.service.exeption.ObjectError;
import com.adsi.service.exeption.ObjectException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class GlobalResponseExceptionHandler {
    @ExceptionHandler(ObjectException.class)
    protected ResponseEntity<Object> handlerObjectException(ObjectException exception){
        log.error(exception.getLocalizedMessage());
        return builResponseException(exception.getObjectError());
    }
    private ResponseEntity<Object>builResponseException(ObjectError objectError){
        return  new ResponseEntity<>(objectError,objectError.getStatus());
    }
}

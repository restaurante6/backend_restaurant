package com.adsi.repository;

import com.adsi.domain.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

    Boolean existsByName (String name);
}

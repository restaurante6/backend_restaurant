package com.adsi.repository;

import com.adsi.domain.Restaurant;
import com.adsi.domain.Turn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TurnRespository extends JpaRepository<Turn, Integer> {
}

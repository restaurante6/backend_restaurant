package com.adsi.web.rest;

import com.adsi.service.BoardService;
import com.adsi.service.DTO.BoardDto;
import com.adsi.service.DTO.Mensaje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/board")
public class BoardResource {

    @Autowired
    BoardService boardService;
@GetMapping("/")
    public List<BoardDto> read() {
        return boardService.read();
    }
@PostMapping("/")
    public ResponseEntity< BoardDto> create(@Valid @RequestBody BoardDto boardDto, BindingResult res) {
    try {
        Map<String, Object> response = new HashMap<>();
    if (boardDto.getIdRestaurante().getId() != 0){
    return new ResponseEntity(boardService.create(boardDto), HttpStatus.OK) ;
}

    }catch (Exception e){
        return new ResponseEntity(new Mensaje("el restaurante no existe"), HttpStatus.NOT_FOUND);
    }
     return new ResponseEntity(new Mensaje("el mesa no se creo"), HttpStatus.NOT_FOUND);
    }

@PutMapping("/")
    public BoardDto update(BoardDto boardDto) {
    return boardService.update(boardDto);
    }

    @GetMapping("/{id}")
    public Optional<BoardDto> getById(int id) {
    return boardService.getById(id);
    }

@DeleteMapping("/{id}")
    public void delete(int id){
    boardService.delete(id);
}
}

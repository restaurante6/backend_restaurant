package com.adsi.web.rest;

import com.adsi.service.DTO.Mensaje;
import com.adsi.service.DTO.RestaurantDto;
import com.adsi.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/restaurant")
public class RestaurantResource {

@Autowired
    RestaurantService restaurantService;

@GetMapping("/")
public List<RestaurantDto> getAllRestaurant() {
    return restaurantService.getAllRestaurant();
}
@PostMapping("/")
public ResponseEntity<RestaurantDto>  create(@RequestBody RestaurantDto restaurantDto) {
    if(restaurantService.existsByName(restaurantDto.getName())){
return new ResponseEntity(new Mensaje("ya existe el restaurante"), HttpStatus.NOT_FOUND);
    }
    RestaurantDto dto =restaurantService.create(restaurantDto);
    return new ResponseEntity(dto, HttpStatus.OK);
}


@GetMapping("/{id}")
    public Optional<RestaurantDto> getById(@PathVariable int id) {
    return restaurantService.getById(id);
    }

@PutMapping("/")
    public RestaurantDto update(RestaurantDto restaurantDto){
    return restaurantService.update(restaurantDto);
    }

@DeleteMapping("{id}")
public void delite(int id){
    restaurantService.delite(id);
}
}

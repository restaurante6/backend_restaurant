package com.adsi.web.rest;

import com.adsi.service.DTO.TurnDto;
import com.adsi.service.TurnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/turn")
public class TurnResource {

@Autowired
    TurnService turnService;

    @PostMapping("/")
    public ResponseEntity<TurnDto> create( @RequestBody TurnDto turnDto){
        /*
        Map<String, Object> response = new HashMap<>();
        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e-> "error en el campo" + e.getField()+""+e.getDefaultMessage())
                    .collect(Collectors.toList());

            response.put("error", errors);
            return new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
        }

         */
        return new ResponseEntity<>(turnService.create(turnDto), HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<Page<TurnDto>>findAll(Pageable pageable){
        return new ResponseEntity<>(turnService.findAll(pageable), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TurnDto> getByid(@PathVariable int id) {
return new ResponseEntity<>(turnService.getByid(id), HttpStatus.OK);
    }
}
